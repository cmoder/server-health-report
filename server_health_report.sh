#!/bin/bash

# XXX: path where the diffs to the logfiles of the last run are written
LASTLOGDIR="/var/local/serverhealth"

# write a headline, surrounded by "===" lines
headline() {
	h="=============================="
	echo -e "$h\n$1\n$h\n"
}

# dmesg entries from the last 24 hours
dmesg_today() {
	dmesg | perl -ale '$. == 1 and $from = $F[0] - 24*3600; /^\[([\d.]+)\]/ and $1 > $from and print' /proc/uptime -
}

# log entries from the last 24 hours
log_last_day() {
	local REGEX="$1"
	local TIMESTR="$2"
	local ADD_YEAR="$3"

	perl -anle '
		BEGIN {
			use Time::Piece ":override";
			use Time::Seconds;
			$g = localtime;
			$oneyear = ($g->is_leap_year ? LEAP_YEAR : NON_LEAP_YEAR);
		}

		/'"$REGEX"'/ and
			$t = Time::Piece->strptime( ('"$ADD_YEAR"' ? $g->year . " $1" : "$1$2"), "'"$TIMESTR"'" ) and
			(($t >= $g - $oneyear ? $t : $t - $oneyear) >= ($g - ONE_DAY + $g->tzoffset)) and
			print'
}

# diff to the saved log entries
logdiff () {
	LOG="$LASTLOGDIR/$1"

	# output modification date of previous log file
	stat --format="Previous version: %y" "$LOG" 2> /dev/null || echo "not found"
	test -s "$LOG" || echo "File empty!" > "$LOG"

	# output old log file; replace it by new one
	# call diff only afterwards, because otherwise it starts reading from the file as soon as the pipe exists, not when the first input comes from the pipe
	perl -e '
		undef $/;
		open( FILE, "+<", "'"$LOG"'" );
		$old = <FILE>;
		seek( FILE, 0, SEEK_SET );
		print FILE <>;
		close FILE;
		open( FILE, "|-", "diff - \"'"$LOG"'\"" );
		print FILE $old;' |
		perl -nle '
			/^[\d,]+([cad])[\d,]+/ and
				$a = ($1 eq "c" ? 1 : 0) and
				map { print shift @p } @p;

			s/^< /> / and
				($a and push(@p, $_) or print) or
			s/^> // and
				(($a and print shift @p), print)'
}


echo -e "Server health report for the last 24 hours\n"


headline "LOGIN ATTEMPTS AUTH.LOG"

# Mail users:
# find /home/ -maxdepth 2 -type d -name Maildir | sed -n -e 's!.*/\([^/]\+\)/Maildir$!\1!p'
# sed -n -e 's/^\([^:]\+\):.*/\1/p' /etc/dovecot/users/

cat /var/log/auth.log.1 /var/log/auth.log |
	log_last_day '^(.{15})' '%Y %b %e %H:%M:%S' 1 |

	perl -MSocket -anle '
	BEGIN {
		print "Successful logins:";

		%loginusers = map { $_->[0] => $_->[1] } map { [(split /:/)[0,-1]] } split /\n/, `grep -v "^root\\|/nologin\$\\|/false\$\\|/sync\$" /etc/passwd`;
		%imapusers = map { s!^.*/([^/]+)/Maildir$!\1! or s!^([^:]+):.*!\1!; $_ => 1 } split "\n", `find /home/ -maxdepth 2 -type d -name Maildir ; cat /etc/dovecot/users`;
	}

	sub reverse_lookup {
		local %p, $h = (gethostbyaddr( inet_aton($_[0]), AF_INET ))[0];
		$h ||= join ", ", grep { /^(netname|descr|country):\s+(.*)/ and not $p{$1}++ and $_ = $2 } `whois "$_[0]" 2> /dev/null`;
	}

	/sshd[:[].*Accepted publickey for (\w+) from ([\d.A-Fa-f:]+)/ and $pub{$1}{$2}++;
	/sshd[:[].*Accepted password for (\w+) from ([\d.A-Fa-f:]+)/ and $pass{$1}{$2}++;

	/su(do)?[:[].*(Successful|session opened) for user (\w+) by (\w+)/ and $sudo{$3}{$4}++;

	/(message repeated (\d+) times: \[ )?Failed password for (invalid user )?(\w+) from ([\d.A-Fa-f:]+)/i and
		$auth{$4} += ($2 > 1 ? $2 : 1) and
		$loginusers{$4} and
		push(@{$loginhosts{$4}}, $5);

	/pam_unix\(dovecot:auth\): authentication failure; logname= uid=0 euid=0 tty=dovecot ruser=(\S+) rhost=(\S+)/ and $dove{$1}++, $imaphosts{$1}{$2}++;

	}{

	print "\nPublic key logins:"; $u = $_, map { print "$u: $pub{$u}{$_}x von $_ (" . reverse_lookup($_) . ")" } sort keys %{$pub{$u}} for sort keys %pub;
	print "\nPassword logins:"; $u = $_, map { print "$u: $pass{$u}{$_}x von $_ (" . reverse_lookup($_) . ")" } sort keys %{$pass{$u}} for sort keys %pass;
	print "\nRoot/sudo sessions:"; $u = $_, map { print "$u: $sudo{$u}{$_}x von $_" } sort keys %{$sudo{$u}} for sort keys %sudo;
	print "\nLogin attempts from users:"; print "$_\t$auth{$_}\tShell: $loginusers{$_} (", join( ", ", map { reverse_lookup($_) } @{$loginhosts{$_}}), ")" for sort { -$auth{$a} <=> -$auth{$b} or $a cmp $b } grep { $loginusers{$_} } keys %auth;
	print "\nMultiple login attempts:"; print "$_\t$auth{$_}" for sort { -$auth{$a} <=> -$auth{$b} or $a cmp $b } grep { $auth{$_} > 1 } grep { ! $loginusers{$_} } keys %auth;
	print "\nSingle login attempts:"; print join ", ", sort grep { $auth{$_} == 1 } grep { ! $loginusers{$_} } keys %auth;
	print "\nIMAP login attempts from users:"; print "$_\t$dove{$_} (", join( ", ", map { reverse_lookup($_) } keys %{$imaphosts{$_}}), ")" for reverse sort { $dove{$a} <=> $dove{$b} or $b cmp $a } grep { $imapusers{$_} } keys %dove;
	print "\nIMAP login attempts:"; print "$_\t$dove{$_}" for reverse sort { $dove{$a} <=> $dove{$b} or $b cmp $a } grep { ! $imapusers{$_} } keys %dove;
	'
echo ""


headline "LOGINS BLOCKED BY FAIL2BAN"

cat /var/log/fail2ban.log.1 /var/log/fail2ban.log |
	log_last_day '^(\d{4}-\d{2}-\d{2} .{8})' '%Y-%m-%d %H:%M:%S' 0 |

	perl -anle '/\] Ban ([\d.]+)/ and $c1{$1}++, $c2++;
	}{
		print "$c2 bans:";
		print "$_->[0]: $_->[1]" for
			grep { $_->[1] =~ /[\d.]+,/ }
			map {
				undef %p;
				[
					$c1{$_},
					join ", ", $_,
					grep { /^(netname|descr|country):\s+(.*)/ and
						not $p{$1}++ and
						$_ = $2
					} `whois "$_" 2> /dev/null`
				] }
			sort { -$c1{$a} <=> -$c1{$b} or $a cmp $b }
			keys %c1'

echo ""


headline "MYSQL STATUS"

{ zcat /var/log/mysql/error.log.1.gz; cat /var/log/mysql/error.log; } |
	log_last_day '^(\d{4}-\d{2}-\d{2}T.{8})' '%Y-%m-%dT%H:%M:%S' 0 |

	perl -nle '/(\[ERROR\].*|\[.* password.*)/i and $c{$1}++
	}{
	print "$c{$_}x: $_" for sort { -$c{$a} <=> -$c{$b} or $a cmp $b } keys %c'
echo ""


headline "KERNEL, UPTIME, LOAD, MEMORY"
uname -a
echo ""
who -b
echo ""
uptime
echo ""
mpstat -P ALL
echo ""
free -h
echo -n "Current user: "; whoami
echo ""


headline "SAR SYSTEM STATISTICS"
sar -1 -F -m ALL -n DEV,IP,TCP,UDP,IP6,UDP6 -P 0 -r -s -u ALL |
	perl -anle '%mm = ( IFACE => [4,5], CPU => [2,3,4], kbmemfree => [3] );
	$. == 1 and print, next;
	/\D$/ and $sect = $F[1] and map { $min{$sect}[$_] = inf } @{$mm{$sect}} and print;
	/^$/ and print;
	/\W(lo|eth1)\W/ and next;
	/^\d+.*\d$/ and map { $min{$sect}[$_] = $F[$_] < $min{$sect}[$_] ? $F[$_] : $min{$sect}[$_]; $max{$sect}[$_] = $F[$_] > $max{$sect}[$_] ? $F[$_] : $max{$sect}[$_]; } @{$mm{$sect}};
	/^Average/ and print and printf "%" . (-10*($mm{$sect}[0])) . "s" . "%11s"x@{$mm{$sect}} . "\n", "min", grep /./, @{$min{$sect}} and printf "%" . (-10*($mm{$sect}[0])) . "s" . "%11s"x@{$mm{$sect}} . "\n", "max", grep /./, @{$max{$sect}}'
echo ""


headline "DISK SPACE"
df -h | grep '^/dev/' | grep -v '/snap/'
echo ""
btrfs fi usage /
echo ""


headline "S.M.A.R.T. HARD DISK STATUS (changes since previous day)"

CRITICAL='^\(  5\| 10\|18[478]\|19[678]\|201\) .*[^0]$'

# XXX: adjust pattern for disk devices
for d in /dev/disk/by-path/pci*-ata-[12]; do
	i="$(readlink -f "$d" | sed -e 's!^/dev/!!')"
	echo -e "/dev/$i:\n---------\n"
	{
		smartctl -i -A -H -l error "/dev/$i" |
		sed -n -e '/Local Time is:/d; 4,/^$/{/^$/!p}; /overall-health/,/^$/p; /^ID/,/^$/p' |
		logdiff "smart_$i.txt" |
		tee /dev/stderr |
		grep "$CRITICAL" |
		sed -n -e "1s!^!Disk /dev/$i ($d):\n\n&!p" |
		# XXX: adjust email sender, receiver and subject
		mail -E -s "[Server] S.M.A.R.T. error on /dev/$i $(date '+%Y-%m-%d %R')" -a 'Content-Type: text/plain; charset="UTF-8"' -a 'X-Binford: 6100 (more power)' -a 'Auto-Submitted: auto-generated' -r 'My Server <root@server.tld>' me@my-email.tld;
	} 2>&1
	echo -e "\nCritical values (!= 0):"
	smartctl -a "/dev/$i" | grep '^\(  5\| 10\|18[478]\|19[678]\|201\) .*[^0]$'
	echo -e "\n"
done


headline "STATE OF THE MDADM ARRAY (changes since previous day)"
{
# XXX: adjust RAID devices (here: 1, 2, 3 == md0, md1 and md2)
for i in 0 1 2; do
	for j in "degraded" "mismatch_cnt"; do
		echo "/sys/block/md$i/md/$j: $(cat "/sys/block/md$i/md/$j")"
	done
	for j in "errors" "bad_blocks"; do
		echo "/sys/block/md$i/md/rd*/$j: $(cat /sys/block/md$i/md/rd*/$j | tr '\n' '  ')"
	done
done
} | logdiff "mdarray.txt"
echo ""


headline "BTRFS FILE SYSTEM (changes since previous day)"
{
	btrfs device stats /
} | logdiff "btrfs.txt"
echo ""


headline "BTRFS ERRORS"

dmesg_today | grep -q 'BTRFS warning (device [[:alnum:]]\+): checksum error' && {
	echo "Files:"
	dmesg_today | grep 'BTRFS warning (device [[:alnum:]]\+): checksum error' | sed -e 's/.*(path: \(.*\))$/\1/' | sort | uniq
	echo ""
	echo "Inodes:"
	dmesg_today | grep 'BTRFS warning (device [[:alnum:]]\+): checksum error' | sed -e 's/.*inode \([^ ,]\+\).*/\1/' | sort | uniq
	echo ""
	echo "Logical:"
	dmesg_today | grep 'BTRFS warning (device [[:alnum:]]\+): checksum error' | sed -e 's/.*logical \([^ ,]\+\).*/\1/' | sort | uniq
	echo ""
	echo "Physical:"
	dmesg_today | grep 'BTRFS warning (device [[:alnum:]]\+): checksum error' | sed -e 's/.*physical \([^ ,]\+\).*/\1/' | sort | uniq
}
echo ""


headline "IO PERFORMANCE"
iostat | grep -v '^loop'
echo ""


headline "NETWORK TRAFFIC VIA IFCONFIG"

# * search for "real" ethernet network interfaces, not e.g. loop interfaces
# * if found: loop until "TX errors" (= last line) has been found, and add subsequent lines
# * then: extract relevant information by regex match
# * print result in one line

ifconfig | perl -nle '
	next unless /^eth|^enp|^br|^wl/;

	$l = <>, $_ = "$_$l" while !/TX errors/;

	$if = (/^([^:]+):/)[0];
	$ip = join ", ", grep { $_ } (/inet\s+([\d.]+)|inet6\s+([[:xdigit:]:]+)[^<]+<link>/g);
	@rxtx = (/([RT]X) packets[^(]+\(([^)]+)\)/g);

	print "$if ($ip): @rxtx\n";'

echo ""


headline "SOCKETS (changes since previous day)"
sed -e '1d' /var/log/checksecurity/sockets.today | logdiff "sockets.txt"
echo ""


headline "SERVER PORTS"
# get IP connections, IPv4 and IPv6
lsof -l -i 4 -i 6 |
	# extract listening ports; store application, protocol (TCP/UDP, IPv4/v6) and ports
	# output: sort local/global, program name, protocol, IP version
	perl -anle '$. - 1 and
		/\(LISTEN\)$/ and
		$p{$F[0]}{"$F[-3]-$F[4]"}{$F[-2]}++
	}{
		print join "\t", @$_ for
			sort { $b->[3] =~ /^\*/ <=> $a->[3] =~ /^\*/ or $a->[1] cmp $b->[1] or $a->[3] cmp $b->[3] or $a->[2] cmp $b->[2] }
			map {
				$a = $_;
				map {
					$b = $_;
					map { ["$p{$a}{$b}{$_}x", $a, $b, $_] }
						keys %{$p{$a}{$_}}
					}
					keys %{$p{$_}}
				}
			keys %p'
echo ""


headline "MAIL.ERR"

cat /var/log/mail.err.1 /var/log/mail.err |
	log_last_day '^(.{15})' '%Y %b %e %H:%M:%S' 1 |
	sed -e 's/.*'"${HOSTNAME%%.*}"' //; s/\[[0-9]\+\]:/:/' | sort | uniq -c | sort -k1nr
echo ""


headline "MYSQL ERROR LOG"
sed -e 's/^[^[]\+\[/[/' /var/log/mysql/error.log | sort | uniq
echo ""


headline "APACHE PHP ERROR LOG (> 2x)"

cat /var/log/apache2/error.log.1 /var/log/apache2/error.log |

	log_last_day '^\[\S+ ([^]]+)\.\d*( \d+)\]' '%b %d %H:%M:%S %Y' 0 |

	perl -nle '/^(\[[^][]+\] )+(.*)/ and $l{$2}++	# extract message and store it
	}{
		print "$l{$_}\t$_" for
			reverse sort { $l{$a} <=> $l{$b} or $b cmp $a }
			grep { (/^PHP Notice:/ or /not found or unable to stat$/ ) ? $l{$_} > 1 : 1 }
			grep { $l{$_} > 2 }
			keys %l'
echo ""


headline "SYSLOG (> 1x)"

cat /var/log/syslog.1 /var/log/syslog |
	log_last_day '^(.{15})' '%Y %b %e %H:%M:%S' 1 |

perl -nle '/error|warn/i and
		/ '"${HOSTNAME%%.*}"' ([^][:]+)(\[\d+\])?(:.*)/ and
		$l{"$1$3"}++
	}{
	print "$l{$_}\t$_" for
		reverse sort { $l{$a} <=> $l{$b} or $b cmp $a }
		grep { !/postfix\/(?:\w+\/)?smtp.+?: warning: hostname \S+ does not resolve to address [\d.]+/ }
		grep { !/postfix\/(?:\w+\/)?smtp.+?: warning: .*?: SASL (?:LOGIN|PLAIN) authentication failed: (?:UGFzc3dvcmQ6)?/ }
		grep { $l{$_} > 1 }
		keys %l'

