Server Health Report
====================

Description
-----------
I found it always hard to understand what’s going on on a computer, especially
if it is a server that is not used interactively. Nobody reads tons of logfiles,
and I found logfile analyzers or intrusion detection systems also not very
helpful.

So I wrote a script that shows me the relevant information on one page; so it
can be run as a cronjob and send me the results via email.


Assumptions
-----------

* Linux system with a current distribution (e.g. Debian stable, Ubuntu LTS).

* Mail server: Postfix and Dovecot

* Web server: Apache 2 with PHP

* MySQL database

* Storage: MDADM RAID, Btrfs filesystem, S.M.A.R.T. analysis

* fail2ban

* Script runs as root or has access to relevant log files and hardware tools.


Usage
-----

This script can almost certainly not be used directly, but has to be customized:

* You are probably using different software or different versions, so the
  parsers must be adjusted. (Yes, the parsing code is complicated, brittle and
  not well documented.)

* Your relevant devices (disks, network interfaces) may have different names.

* Some sections may be irrelevant (e.g. mdadm RAID), others missing.

* Paths and user names have to be adjusted.

=> Look especially for the lines marked by “XXX”, and adjust them.

So this script is not suitable for inexperienced users, but rather for people
who know what they are doing and take this script as an inspiration or starting
point for their own solutions.


License
-------
[*Creative Commons CC-BY*](https://creativecommons.org/licenses/by/4.0/deed.de)
– i.e. do what you want, but please mention my name.
